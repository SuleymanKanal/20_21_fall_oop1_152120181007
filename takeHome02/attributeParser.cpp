#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;

bool isContaining(map<string, string> contains, string key, string value) {
    for (auto it = contains.begin(); it != contains.end(); ++it)
        if (it->first == key && it->second == value)
            return true;
    return false;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n, q;
    unsigned int size;
    cin >> n >> q;
    string* hrml = new string[n];
    string* query = new string[q];
    cin.ignore();
    for (int i = 0; i < n; i++)
    {
        getline(cin, hrml[i]);
    }

    for (int i = 0; i < q; i++)
    {
        getline(cin, query[i]);
    }

    map<string, map<string, string> > tags;
    string key, value, tag, nextTag, nextTemp;
    int index, index2 = 1;;
    map<string, string> contains;
    for (int i = 0; i < n; i++)
    {
        size = hrml[i].size();
        string temp = hrml[i];
        if (i != n - 1)
            nextTemp = hrml[i + 1];
        for (int j = 0; j < size; j++)
        {
            if (temp[j] == '<' && temp[j + 1] != '/')
            {
                index = j + 1;
                while (temp[index] != ' ')
                {
                    tag += temp[index];
                    index++;
                }
            }
            if (i != n - 1)
            {
                if (isalnum(hrml[i + 1][1]))
                {
                    while (nextTemp[index2] != ' ')
                    {
                        nextTag += nextTemp[index2];
                        index2++;
                    }
                    contains.insert(make_pair(tag, nextTag));
                }
            }

            if (temp[j] == ' ' && isalpha(temp[j + 1]))
            {
                index = j + 1;
                while (temp[index] != ' ')
                {
                    key += temp[index];
                    index++;
                }
            }

            if (temp[j] == '\"')
            {
                index = j + 1;
                while (temp[index] != '\"')
                {
                    value += temp[index];
                    index++;
                }
                j = index;
            }
        }
        tags[tag].insert(make_pair(key, value));
        key = "";
        value = "";
        tag = "";
        nextTag = "";
    }

    string qtag, nextqtag;
    string qvalue;
    for (int i = 0; i < q; i++)
    {
        size = query[i].size();
        string temp = query[i];
        int iter = 0;
        while (temp[iter] != '.' && temp[iter] != '~')
        {
            qtag += temp[iter];
            iter++;
        }

        for (int k = iter; k < size; k++)
        {

            if (temp[k] == '.')
            {
                iter = k + 1;
                while (temp[iter] != '.' && temp[iter] != '~')
                {
                    nextqtag += temp[iter];
                    iter++;
                }

                if (isContaining(contains, qtag, nextqtag))
                {
                    qtag = nextqtag;
                }
                else
                {
                    qtag = "";
                    break;
                }

            }
            else if (temp[k] == '~')
            {
                while (isalpha(temp[iter + 1]))
                {
                    qvalue += temp[iter + 1];
                    iter++;
                }
                break;
            }
        }

        if (tags[qtag][qvalue] == "")
            cout << "Not Found!" << endl;
        else
            cout << tags[qtag][qvalue] << endl;
        qtag = "";
        qvalue = "";
    }

    return 0;
}

