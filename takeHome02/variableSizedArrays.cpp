#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n, q, k, i, j;
    cin >> n >> q;
    int* output = new int[q];
    int** a = new int* [n];
    for (int x = 0; x < n; x++)
    {
        cin >> k;
        a[x] = new int[k];
        for (int y = 0; y < k; y++)
        {
            cin >> a[x][y];
        }
    }

    for (int x = 0; x < q; x++)
    {
        cin >> i >> j;
        output[x] = a[i][j];
    }

    for (int x = 0; x < q; x++)
    {
        cout << output[x] << endl;
    }
    return 0;
}
