#include <iostream>
#include <fstream>
using namespace std;

int sum(int* numbers, int numOfInts);
int product(int* numbers, int numOfInts);
int min(int* numbers, int numOfInts);

void main() {

	fstream file;   //! Input/output stream class named file to operate on files.
	file.open("input.txt", ios::in);	//! Opens file to read inside of it

	if (!file)	//! Checks if the file could not open. If it's not, write an error message to screen and exit from the program.
	{
		cout << "Error, File not opened!" << endl;
		exit(0);
	}

	int numOfInts;	//! Int variable to restore the first line of the file. That line will determine the size of the array.
	file >> numOfInts;	//! Put the first line of the file into the numOfInts
	int* numbers = new int[numOfInts];	//! Creates a dynamic array according to the first line of the file
	int i = 0;	//! Iteration number to restore the integer values in the array named numbers
	while (!file.eof())
	{
		file >> numbers[i];	//! Put the integer values in the array.
		i++;
	}

	cout << "Sum is " << sum(numbers, numOfInts) << endl;	//! Prints the returned value from sum function.
	cout << "Product is " << product(numbers, numOfInts) << endl;	//! Prints the returned value from product function.
	/**
	* Converting the integer value which returned from the sum function and numOfInt variable to float.
	* Dividing the returned value to numOfInt variable to calculate the average.
	*/
	cout << "Avreage is " << float(sum(numbers, numOfInts)) / float(numOfInts) << endl;
	cout << "Smallest is " << min(numbers, numOfInts) << endl;	//! Prints the returned value from min function.

	file.close();
	system("pause");
}

/**
* Sum Function
* Takes the size of the array as a parameter to determine the iteration limit in the loop
* Takes the generated array as a parameter
* Adds the integer values which is inside the array and return the sum value.
*/
int sum(int* numbers, int numOfInts)
{
	int sum = 0;
	for (int i = 0; i < numOfInts; i++)
	{
		sum += numbers[i];
	}

	return sum;
}


/**
* Product Function
* Takes the size of the array as a parameter to determine the iteration limit in the loop
* Takes the generated array as a parameter
* Multiplies the integer values which is inside the array and return the product value.
* Inýtialize the product variable from 1, unlike the summation function.
*/
int product(int* numbers, int numOfInts)
{
	int product = 1;
	for (int i = 0; i < numOfInts; i++)
	{
		product *= numbers[i];
	}

	return product;
}

/**
* Min Function
* Takes the size of the array as a parameter to determine the iteration limit in the loop
* Takes the generated array as a parameter
* Takes the first element of the array as the minimum value
* If the next element of the array smaller than the current minimum value, makes it new minimum value.
* Returns the last minimum value
*/
int min(int* numbers, int numOfInts)
{
	int min = numbers[0];
	for (int i = 1; i < numOfInts; i++)
	{
		if (numbers[i] < min)
			min = numbers[i];
	}

	return min;
}